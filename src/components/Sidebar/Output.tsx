import React, { Component } from 'react';
import { List } from 'immutable';

import { Group, GeneratedDataFrame } from '../../data/types';

import styles from './Sidebar.module.css';

interface OutputProps {
  generatedData?: List<GeneratedDataFrame>;

  groups: List<Group>;
  selectedGroupIndex: number;
}
interface OutputState {
  dataIndex: number;
}

export default class Output extends Component<OutputProps, OutputState> {
  state = {
    dataIndex: 0
  }

  laterItem = () => {
    this.setState((prevState) => {
      if (this.props.generatedData && prevState.dataIndex < this.props.generatedData.size - 1) {
        return {
          dataIndex: prevState.dataIndex + 1
        };
      }
    });
  }

  earlierItem = () => {
    this.setState((prevState) => {
      if (this.props.generatedData && prevState.dataIndex > 0) {
        return {
          dataIndex: prevState.dataIndex - 1
        };
      }
    });
  }

  render() {
    const generatedData = this.props.generatedData;
    if (!generatedData) {
      return null;
    }

    const dataIndex = this.state.dataIndex;
    const data = generatedData.get(this.state.dataIndex);

    const group = this.props.groups.get(this.props.selectedGroupIndex);

    return (
      <div className={styles.flexWrapper}>
        <div className={styles.outputHeading}>
          <h3>
            Data { group && `(${group.mouse.name})` }
          </h3>
          <div>
            <button onClick={this.laterItem}>&#8592;</button>
            {/* Display the current frame number in reverse (the higher numbers are the latest events) */}
            { generatedData.size - dataIndex } / { generatedData.size }
            <button onClick={this.earlierItem}>&#8594;</button>
          </div>
        </div>
        <div className={styles.expandingScrollArea}>
          {
            data && (
              <div>
                <code>Timestamp: { data.timestamp.format("L LTS") }</code>
                <div>
                  <strong>Mouse:</strong>
                  <code>
                    {
                      JSON.stringify(data.data.mouse, null, 2)
                    }
                  </code>
                </div>
                <div>
                  <strong>Cats:</strong>
                  {
                    data.data.cats.map((cat) => (
                      <code key={cat.properties.id}>
                        { JSON.stringify(cat, null, 2) }
                      </code>
                    ))
                  }
                </div>
              </div>
            )
          }
        </div>
      </div>
    );
  }
}
