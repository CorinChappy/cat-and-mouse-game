import React, { Component, FormEvent, RefObject } from 'react';
import { List } from 'immutable';
import { LatLngExpression, latLng } from 'leaflet';
import classnames from 'classnames';

import { Group } from '../../data/types';
import { getRandomNumber, GenerateGroupOptions } from '../../data';

import styles from './Sidebar.module.css';


function getRandomMouseId(groups: List<Group>) {
  let num: string;
  do {
    num = String(Math.floor(getRandomNumber(100, 99999)));
  } while (groups.some(({ mouse }) => mouse.id === num))

  return num;
}

interface OptionsProps {
  groups: List<Group>;

  selectedGroupIndex: number;
  onSelectedGroupChange: (groupIndex: number) => void;

  createNewGroup: (opts: GenerateGroupOptions) => void;

  currentMarkerLocation?: LatLngExpression;
}
interface OptionsState {
  newGroupForm: {
    mouseId: string;
    mouseName: string;
    numberOfCats: string;
    catNamePrefix: string;
  }
}

export default class Options extends Component<OptionsProps, OptionsState> {

  constructor(props: OptionsProps) {
    super(props);

    this.state = {
      newGroupForm: {
        mouseId: getRandomMouseId(props.groups),
        mouseName: "",
        numberOfCats: "5",
        catNamePrefix: "CAT"
      }
    }
  }

  nameRef = React.createRef<HTMLInputElement>();

  updateNewGroupForm = (
    field: "mouseId" | "mouseName" | "numberOfCats" | "catNamePrefix",
    evt: FormEvent<HTMLInputElement>
  ) => {
    const vaule = evt.currentTarget.value.toUpperCase();

    this.setState((prevState) => ({
      newGroupForm: {
        ...prevState.newGroupForm,
        [field]: vaule
      }
    }));
  }

  createNewGroup = () => {
    const newGroupForm = this.state.newGroupForm;
    const currentMarkerLocation = this.props.currentMarkerLocation;
    if (!currentMarkerLocation || !newGroupForm.mouseId || !newGroupForm.mouseName) {
      return;
    }

    this.props.createNewGroup({
      mouse: {
        id: newGroupForm.mouseId,
        name: newGroupForm.mouseName
      },
      startPosition: currentMarkerLocation,
      numberOfCats: newGroupForm.numberOfCats ? parseInt(newGroupForm.numberOfCats, 10) : undefined,
      catNamePrefix: newGroupForm.catNamePrefix ? newGroupForm.catNamePrefix : undefined
    });

    this.setState((prevState) => ({
      newGroupForm: {
        ...prevState.newGroupForm,
        mouseId: getRandomMouseId(this.props.groups),
        mouseName: ""
      }
    }));

    this.nameRef.current && this.nameRef.current.focus();
  }

  render() {
    const newGroupForm = this.state.newGroupForm;
    return (
      <div className={styles.flexWrapper}>
        <h3 className={styles.rule}>Options</h3>

        <div className={classnames(styles.newGroupForm, styles.rule)}>
          <h4>Create new group</h4>
          <div>
            <strong>Mouse</strong>
            <div>
              ID: <input placeholder="Required" value={newGroupForm.mouseId} onChange={ this.updateNewGroupForm.bind(null, "mouseId") } />
              <br/>
              Name: <input ref={this.nameRef} placeholder="Required" value={newGroupForm.mouseName} onChange={this.updateNewGroupForm.bind(null, "mouseName")} />
            </div>
          </div>
          <div>
            <strong>Cats</strong>
            <div>
              Number of cats: <input placeholder="5" type="number" value={newGroupForm.numberOfCats} onChange={this.updateNewGroupForm.bind(null, "numberOfCats")} />
              <br/>
              Cat name prefix: <input placeholder="CAT" value={newGroupForm.catNamePrefix} onChange={this.updateNewGroupForm.bind(null, "catNamePrefix")} />
            </div>
          </div>
          <div>
            <strong>Mouse will be placed at:</strong>
            <code>
              {
                this.props.currentMarkerLocation ? (
                  latLng(this.props.currentMarkerLocation).toString()
                ) : "LatLng(???, ???)"
              }
            </code>
          </div>
          <div>
            <button onClick={ this.createNewGroup }>Create Group</button>
          </div>
        </div>


        <h3>Current groups</h3>
        <div className={styles.expandingScrollArea}>
          {
            this.props.groups.map((group, i) => (
              <div key={group.mouse.id}>
                <hr />

                <div>
                  <strong>Mouse</strong> <a onClick={ () => this.props.onSelectedGroupChange(i) }>Select</a>
                  <div>
                    ID: { group.mouse.id }<br />
                    Name: { group.mouse.name }
                  </div>
                </div>
                <div>
                  <strong>Cats</strong>
                  {
                    group.cats.map((cat) => (
                      <div key={cat.id}>
                        {cat.id} - {cat.name}
                      </div>
                    ))
                  }
                </div>
              </div>
            ))
          }
        </div>
      </div>
    );
  }
}
