import React, { Component } from 'react';
import { List } from 'immutable';
import { LatLngExpression } from 'leaflet';

import { Group, GeneratedDataFrame } from '../../data/types';
import { GenerateGroupOptions } from '../../data';

import Options from './Options';
import Output from './Output';

import styles from './Sidebar.module.css';

enum SidebarTab {
  Options,
  Output
}
interface SidebarProps {
  generatedData: List<List<GeneratedDataFrame>>;

  groups: List<Group>;
  selectedGroupIndex: number;
  onSelectedGroupChange: (groupIndex: number) => void;

  createNewGroup: (opts: GenerateGroupOptions) => void;

  currentMarkerLocation?: LatLngExpression;
}
interface SidebarState {
  currentTab: SidebarTab
}

export default class Sidebar extends Component<SidebarProps, SidebarState> {
  state: SidebarState = {
    currentTab: SidebarTab.Options
  }

  changeTab = (tab: SidebarTab) => {
    this.setState({ currentTab: tab });
  }

  render() {
    const selectedGroupIndex = this.props.selectedGroupIndex;
    return (
      <div className={styles.Sidebar}>
        <div className={styles.tabs}>
          <button onClick={ () => this.changeTab(SidebarTab.Options) }>Options</button>
          <button onClick={ () => this.changeTab(SidebarTab.Output) }>Output</button>
        </div>
        <div className={styles.tabContent}>
          {
            this.state.currentTab === SidebarTab.Options && (
              <Options
                groups={this.props.groups}
                selectedGroupIndex={selectedGroupIndex}
                onSelectedGroupChange={this.props.onSelectedGroupChange}
                createNewGroup={this.props.createNewGroup}
                currentMarkerLocation={this.props.currentMarkerLocation}
              />
            )
          }
          {
            this.state.currentTab === SidebarTab.Output && (
              <Output
                generatedData={this.props.generatedData.get(selectedGroupIndex)}
                groups={this.props.groups}
                selectedGroupIndex={selectedGroupIndex}
              />
            )
          }
        </div>
      </div>
    );
  }
}
