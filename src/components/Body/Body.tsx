import React, { Component } from 'react';
import { List } from 'immutable';
import { LatLngExpression } from 'leaflet';

import { Group, CompassDirection, GeneratedDataFrame } from '../../data/types';
import { DEFAULT_CENTRE } from '../../constants';

import Sidebar from '../Sidebar/Sidebar';
import Map from '../Map/Map';
import CompassOverlay from '../CompassOverlay/CompassOverlay';

import { generateGroup, moveMouse, GenerateGroupOptions } from '../../data';
import { generateDataFrame } from '../../data/generate';
import { sendDataToApi } from '../../api/api';

import styles from './Body.module.css';

interface BodyProps {}
interface BodyState {
  groups: List<Group>;
  selectedGroupIndex: number;

  generatedData: List<List<GeneratedDataFrame>>;

  currentMarkerLocation?: LatLngExpression;
}

export default class Body extends Component<BodyProps, BodyState> {

  constructor(props: BodyProps) {
    super(props);

    const groups = List();
    const generatedData = groups.map((group) => List([generateDataFrame(group)]));

    this.state = {
      selectedGroupIndex: 0,
      groups,
      generatedData,

      currentMarkerLocation: DEFAULT_CENTRE
    }
  }

  createNewGroup = (opts: GenerateGroupOptions) => {
    const newGroup = generateGroup(opts);
    const newFrame = generateDataFrame(newGroup);

    this.setState((prevState) => ({
      groups: prevState.groups.push(newGroup),
      generatedData: prevState.generatedData.push(List([newFrame])),
      selectedGroupIndex: prevState.groups.size
    }));
  }

  moveMouseInGroup = (group: Group, direction: CompassDirection) => {
    const groups = this.state.groups;
    const index = groups.indexOf(group);
    if (index === -1) {
      return;
    }

    const newGroup = moveMouse({ group, direction });
    const newGroups = groups.set(index, newGroup);

    const newFrame = generateDataFrame(newGroup);
    const newData = this.state.generatedData.update(index, List(), (val = List()) => val.unshift(newFrame));

    sendDataToApi(newFrame);

    this.setState({
      groups: newGroups,
      generatedData: newData
    });
  }

  changeSelectedGroup = (groupIndex: number) => {
    this.setState({
      selectedGroupIndex: groupIndex
    });
  }

  updateMarkerLocation = (latlng: LatLngExpression) => {
    this.setState({
      currentMarkerLocation: latlng
    });
  }

  render() {
    const {
      groups,
      selectedGroupIndex
    } = this.state;

    return (
      <div className={styles.Body}>
        <div className={styles.fullPageWrapper}>
          <div className={styles.mapWrapper}>
            <Map
              onSelectedGroupChange={this.changeSelectedGroup}
              groups={groups}
              onActorMove={this.moveMouseInGroup}
              currentMarkerLocation={this.state.currentMarkerLocation}
              onMarkerLocationUpdate={this.updateMarkerLocation}
            />
          </div>

          <div className={styles.sidebarWrapper}>
            <Sidebar
              generatedData={this.state.generatedData}
              groups={groups}
              selectedGroupIndex={selectedGroupIndex}
              onSelectedGroupChange={this.changeSelectedGroup}
              createNewGroup={this.createNewGroup}
              currentMarkerLocation={this.state.currentMarkerLocation}
            />
          </div>
        </div>

        <div className={styles.compassWrapper}>
          <CompassOverlay
            selectedGroupIndex={selectedGroupIndex}
            onSelectedGroupChange={this.changeSelectedGroup}
            groups={groups}
            onActorMove={this.moveMouseInGroup}
          />
        </div>
      </div>
    );
  }
}
