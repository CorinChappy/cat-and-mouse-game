import React, { Component, FormEvent } from 'react';
import { List } from 'immutable';

import { Group, CompassDirection } from '../../data/types';

import styles from './CompassOverlay.module.css';


interface CompassOverlayProps {
  selectedGroupIndex: number;
  onSelectedGroupChange: (groupIndex: number) => void;
  groups: List<Group>;
  onActorMove: (group: Group, direction: CompassDirection) => void;
}

export default class CompassOverlay extends Component<CompassOverlayProps> {

  // We can also use compass overlay to bind to keyboard events
  componentDidMount() {
    window.addEventListener("keyup", this.keyListener);
  }

  componentWillUnmount() {
    window.removeEventListener("keyup", this.keyListener);
  }

  keyListener = (e: KeyboardEvent) => {
    switch(e.key) {
      case "ArrowUp": return this.makeMove(CompassDirection.NORTH);
      case "ArrowDown": return this.makeMove(CompassDirection.SOUTH);
      case "ArrowLeft": return this.makeMove(CompassDirection.WEST);
      case "ArrowRight": return this.makeMove(CompassDirection.EAST);
    }
  }

  makeMove = (direction: CompassDirection) => {
    const group = this.props.groups.get(this.props.selectedGroupIndex);
    if (group) {
      this.props.onActorMove(group, direction);
    }
  }

  groupChanged = (event: FormEvent<HTMLSelectElement>) => {
    this.props.onSelectedGroupChange(parseInt(event.currentTarget.value, 10));
  }

  render() {
    return (
      <div className={styles.CompassOverlay}>
        <div>
          <strong>Select Group</strong>
          <select className={styles.groupSelect} value={this.props.selectedGroupIndex} onChange={this.groupChanged}>
            {
              this.props.groups.map((group, i) => (
                <option key={i} value={i}>{group.mouse.name}</option>
              ))
            }
          </select>

          <strong>Move Group</strong>
          <div className={styles.compass}>
            <div>
              <button onClick={() => this.makeMove(CompassDirection.NORTH_WEST)}>NORTH WEST</button>
              <button onClick={() => this.makeMove(CompassDirection.WEST)}>WEST</button>
              <button onClick={() => this.makeMove(CompassDirection.SOUTH_WEST)}>SOUTH WEST</button>
            </div>
            <div>
              <button onClick={() => this.makeMove(CompassDirection.NORTH)}>NORTH</button>
              <div>&nbsp;</div>
              <button onClick={() => this.makeMove(CompassDirection.SOUTH)}>SOUTH</button>
            </div>
            <div>
              <button onClick={() => this.makeMove(CompassDirection.NORTH_EAST)}>NORTH EAST</button>
              <button onClick={() => this.makeMove(CompassDirection.EAST)}>EAST</button>
              <button onClick={() => this.makeMove(CompassDirection.SOUTH_EAST)}>SOUTH EAST</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
