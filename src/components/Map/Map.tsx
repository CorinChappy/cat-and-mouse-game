import React, { Component } from 'react';
import { List } from 'immutable';
import classnames from 'classnames';

import { latLng, LatLngExpression, Icon, LeafletMouseEvent } from 'leaflet';
import { Map as LeafletMap, TileLayer, FeatureGroup, CircleMarker, Popup, Rectangle, Marker } from 'react-leaflet';
import Control from 'react-leaflet-control';
import Tether from 'react-tether';

// Import the leaflet CSS
import 'leaflet/dist/leaflet.css';
import iconRetinaUrl from 'leaflet/dist/images/marker-icon-2x.png';
import iconUrl from 'leaflet/dist/images/marker-icon.png';
import shadowUrl from 'leaflet/dist/images/marker-shadow.png';

import styles from './Map.module.css';
import { DEFAULT_CENTRE, DEFAULT_ZOOM, CAT_DISTANCE_MIN, CAT_DISTANCE_MAX } from '../../constants';
import { Group, CompassDirection } from '../../data/types';

// Patch to get Icons to work properly
delete (Icon.Default as any).prototype._getIconUrl;

Icon.Default.mergeOptions({
  iconRetinaUrl,
  iconUrl,
  shadowUrl
});


const SHOW_BOUNDING_BOXES = false;

interface MapProps {
  onSelectedGroupChange: (groupIndex: number) => void;

  groups: List<Group>;
  onActorMove: (group: Group, direction: CompassDirection) => void;

  currentMarkerLocation?: LatLngExpression;
  onMarkerLocationUpdate: (latlng: LatLngExpression) => void;
}
interface MapState {
  moveMarkerEnabled: boolean;
}

export default class Map extends Component<MapProps, MapState> {
  state = {
    moveMarkerEnabled: false
  }

  timeoutId?: number;

  componentDidMount() {
    // Timeout required to force an update on the component
    // otherwise the Control component will not render
    // until next state change
    this.timeoutId = window.setTimeout(() => {
      this.forceUpdate();
    }, 0);

    window.addEventListener("keydown", this.keyDownListener);
  }

  componentWillUnmount() {
    window.clearTimeout(this.timeoutId);
    window.removeEventListener("keydown", this.keyDownListener);
  }

  keyDownListener = (e: KeyboardEvent) => {
    if (e.key === "Escape") {
      this.setState({
        moveMarkerEnabled: false
      });
    }
  }

  toggleMoveMarkerEnabled = () => {
    this.setState((prevState) => ({
      moveMarkerEnabled: !prevState.moveMarkerEnabled
    }));
  }

  potentuallyMoveMarkerLocation = (e: LeafletMouseEvent) => {
    if (this.state.moveMarkerEnabled) {
      this.props.onMarkerLocationUpdate(e.latlng);
    }
  }

  render() {
    const props = this.props;

    return (
      <LeafletMap className={styles.leafletMap} center={DEFAULT_CENTRE} zoom={DEFAULT_ZOOM} onClick={this.potentuallyMoveMarkerLocation}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />

        <Control position="topleft">
          <Tether attachment="middle left" style={{ zIndex: 9999 }}>
            <div className="leaflet-bar">
              <a
                className={classnames(styles.control, { [styles.enabled]: this.state.moveMarkerEnabled })}
                onClick={this.toggleMoveMarkerEnabled}
                role="button"
                aria-label="Change new group location"
                title="Change new group location"
              >
                <img src={iconUrl} />
              </a>
            </div>

            {
              this.state.moveMarkerEnabled && (
                <div className={styles.controlTooltip}>
                  <div>
                    Click somewhere on the map<br/>
                    to move the marker
                  </div>
                </div>
              )
            }
          </Tether>
        </Control>

        {
          SHOW_BOUNDING_BOXES && props.groups.map((group) => {
            const latlng = latLng(group.mouse.position);
            // Obtain bounding boxes for the min and max areas
            const minBoundingBox = latlng.toBounds(CAT_DISTANCE_MIN * 2);
            const maxBoundingBox = latlng.toBounds(CAT_DISTANCE_MAX * 2);

            return (
              <React.Fragment>
                <Rectangle bounds={minBoundingBox} color="green" />
                <Rectangle bounds={maxBoundingBox} color="purple" />
              </React.Fragment>
            );
          })
        }

        {
          props.groups.map((group, i) => (
            <FeatureGroup key={group.mouse.id}>
              <CircleMarker center={group.mouse.position} radius={5} fillOpacity={1} color="red" onClick={ () => props.onSelectedGroupChange(i) }>
                <Popup>
                  <h4>Mouse</h4>
                  <ul>
                    <li>ID: {group.mouse.name}</li>
                    <li>Name: {group.mouse.name}</li>
                  </ul>
                </Popup>
              </CircleMarker>
              {
                group.cats.map((cat) => (
                  <CircleMarker key={cat.id} center={cat.position} radius={5} fillOpacity={1} color="blue">
                    <Popup>
                      <h4>Cat</h4>
                      <ul>
                        <li>ID: {cat.name}</li>
                        <li>Name: {cat.name}</li>
                        <li>Hunting: {group.mouse.name}</li>
                      </ul>
                    </Popup>
                  </CircleMarker>
                ))
              }
            </FeatureGroup>
          ))
        }


        {
          props.currentMarkerLocation && (
            <Marker position={props.currentMarkerLocation} >
              <Popup>
                A new group will be placed at this location
              </Popup>
            </Marker>
          )
        }
      </LeafletMap>
    );
  }
}
