import { GeneratedDataFrame } from "../data/types";


export function sendDataToApi(frame: GeneratedDataFrame): Promise<void> {
  // For now, the data will just be logged to the console
  console.log(frame);

  return Promise.resolve();
}
