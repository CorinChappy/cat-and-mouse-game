import { CRS, LatLngExpression } from "leaflet";

// Default CRS used byleaflet
export const crs = CRS.EPSG3857;

export const DEFAULT_CENTRE: LatLngExpression = [51.505, -0.09];
export const DEFAULT_ZOOM = 12;

export const UK_CENTRE: LatLngExpression = [54.559322, -4.1748039];
export const UK_ZOOM = 6;


// 5km
export const CAT_DISTANCE_MIN = 5000;
// 10km
export const CAT_DISTANCE_MAX = 10000;

// 1km
export const DEFAULT_MOVE_DISTANCE = 500;
