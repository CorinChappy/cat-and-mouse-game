declare module 'react-leaflet-control' {
  export interface ControlProps {
    position: "topleft" | "topright" | "bottomleft" | "bottomright";
    children: React.ReactNode
  }
  export default class Control extends React.Component<ControlProps> {}
}
