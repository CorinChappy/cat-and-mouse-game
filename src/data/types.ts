import { LatLngExpression } from 'leaflet';
import { Point, Feature } from 'geojson';
import { List } from 'immutable';
import { Moment } from 'moment';

export interface Actor {
  id: string;
  name: string;
  position: LatLngExpression;
}

export interface Group {
  mouse: Actor;
  cats: List<Actor>;
}

export interface GeneratedDataObjectProperties {
  id: string;
  name: string;
}
export interface GeneratedDataFeature extends Feature<Point, GeneratedDataObjectProperties> {}

export interface GeneratedDataObject {
  mouse: GeneratedDataFeature;
  cats: List<GeneratedDataFeature>;
}

export interface GeneratedDataFrame {
  timestamp: Moment;
  data: GeneratedDataObject;
}

export enum CompassDirection {
  NORTH,
  NORTH_EAST,
  EAST,
  SOUTH_EAST,
  SOUTH,
  SOUTH_WEST,
  WEST,
  NORTH_WEST
}
