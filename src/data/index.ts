import { LatLngExpression, latLng, LatLngBounds } from "leaflet";
import { Group, CompassDirection } from "./types";
import { CAT_DISTANCE_MIN, CAT_DISTANCE_MAX, DEFAULT_MOVE_DISTANCE } from "../constants";

import { List } from 'immutable';


export interface GenerateGroupOptions {
  startPosition: LatLngExpression;
  mouse: {
    id: string;
    name: string;
  };
  numberOfCats?: number;
  catNamePrefix?: string;
}
export function generateGroup(opts: GenerateGroupOptions): Group {
  const { startPosition, mouse, numberOfCats = 5, catNamePrefix = "CAT" } = opts;

  return {
    mouse: {
      ...mouse,
      position: startPosition
    },
    cats: List(Array(numberOfCats).fill(0).map((_, i) => ({
      id: String(i),
      name: `${catNamePrefix}_${i}`,
      position: getInitialLocationOfCat(startPosition)
    })))
  }
}

export function getBoundingBoxes(position: LatLngExpression, minDistance: number, maxDistance: number): [LatLngBounds, LatLngBounds] {
  // Obtain bounding boxes for the min and max areas
  const latlng = latLng(position);
  const minBoundingBox = latlng.toBounds(minDistance * 2);
  const maxBoundingBox = latlng.toBounds(maxDistance * 2);

  return [minBoundingBox, maxBoundingBox];
}

export function getInitialLocationOfCat(startPosition: LatLngExpression, minDistance: number = CAT_DISTANCE_MIN, maxDistance: number = CAT_DISTANCE_MAX): LatLngExpression {
  const [minBoundingBox, maxBoundingBox] = getBoundingBoxes(startPosition, minDistance, maxDistance);

  // Pick two sides: top or bottom & left or right
  let minLat, maxLat, minLng, maxLng;
  if (Math.random() > 0.5) {
    minLng = maxBoundingBox.getWest();
    maxLng = minBoundingBox.getWest();
  } else {
    minLng = minBoundingBox.getEast();
    maxLng = maxBoundingBox.getEast();
  }
  if (Math.random() > 0.5) {
    minLat = maxBoundingBox.getNorth();
    maxLat = minBoundingBox.getNorth();
  } else {
    minLat = minBoundingBox.getSouth();
    maxLat = maxBoundingBox.getSouth();
  }

  // Get a random lat and lng, using those boxes
  return [getRandomNumber(minLat, maxLat), getRandomNumber(minLng, maxLng)];
}

export function getRandomNumber(min: number, max: number): number {
  return Math.random() * (max - min) + min;
}


function getRandomDirection(): CompassDirection {
  return Math.floor(Math.random() * Object.keys(CompassDirection).length / 2);
}

interface MoveMouseOptions {
  group: Group;
  direction: CompassDirection;
  distance?: number;
  minDistance?: number;
  maxDistance?: number;
}
export function moveMouse({ group, direction, distance = DEFAULT_MOVE_DISTANCE, minDistance = CAT_DISTANCE_MIN, maxDistance = CAT_DISTANCE_MAX }: MoveMouseOptions): Group {
  // Move the mouse
  const newMousePosition = adjustPosition(group.mouse.position, direction, distance);
  const latlng = latLng(newMousePosition);
  // Obtain bounding boxes for the min and max areas
  const minBoundingBox = latlng.toBounds(minDistance * 2);
  const maxBoundingBox = latlng.toBounds(maxDistance * 2);

  // Move each of the cats
  const newCats = group.cats.map((cat) => {
    // Adjust the position of the cat in a random direction
    let newPosition: LatLngExpression;
    do {
      const dir = getRandomDirection();
      const moveDisatance = distance + getRandomNumber(0, distance / 2);
      newPosition = adjustPosition(cat.position, dir, moveDisatance);
    } while (minBoundingBox.contains(newPosition) || !maxBoundingBox.contains(newPosition));

    return {
      ...cat,
      position: newPosition
    };
  });

  return {
    mouse: {
      ...group.mouse,
      position: newMousePosition
    },
    cats: newCats
  }
}

function adjustPosition(position: LatLngExpression, direction: CompassDirection, distance: number): LatLngExpression {
  // Create a bounding box here
  const latlng = latLng(position);
  const bounds = latlng.toBounds(distance * 2);

  if ([CompassDirection.NORTH, CompassDirection.EAST, CompassDirection.SOUTH, CompassDirection.WEST].includes(direction)) {
    switch (direction) {
      case CompassDirection.NORTH: return [bounds.getNorth(), latlng.lng];
      case CompassDirection.SOUTH: return [bounds.getSouth(), latlng.lng];
      case CompassDirection.EAST: return [latlng.lat, bounds.getEast()];
      case CompassDirection.WEST: return [latlng.lat, bounds.getWest()];

      default: return bounds.getCenter()
    }
  } else {
    // Calculate something more complicated
    switch (direction) {
      case CompassDirection.NORTH_EAST: return bounds.getNorthEast();
      case CompassDirection.SOUTH_EAST: return bounds.getSouthEast();
      case CompassDirection.SOUTH_WEST: return bounds.getSouthWest();
      case CompassDirection.NORTH_WEST: return bounds.getNorthWest();

      default: return bounds.getCenter()
    }
  }
}
