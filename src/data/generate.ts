import moment from "moment";
import { latLng, GeoJSON } from 'leaflet';

import { Group, GeneratedDataFrame, GeneratedDataFeature, Actor } from "./types";

export function generateDataFrame(group: Group): GeneratedDataFrame {
  return {
    timestamp: moment(),
    data: {
      mouse: generateDataFeature(group.mouse),
      cats: group.cats.map(generateDataFeature)
    }
  }
}

export function generateDataFeature(actor: Actor): GeneratedDataFeature {
  return {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: GeoJSON.latLngToCoords(latLng(actor.position))
    },
    properties: {
      id: actor.id,
      name: actor.name
    }
  }
}
